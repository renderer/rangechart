angular.module('dongbat.rangechart', []);

(function() {

  var module = angular.module('dongbat.rangechart');
  module.directive('rangechart', function() {
    return {
      restrict: 'E',
      template: "<canvas width=\"{{width}}\" height=\"{{height + 10}}\"></canvas>",
      scope: {
        width: '=',
        height: '=',
        open: '=',
        close: '=',
        high: '=',
        low: '=',
        color: '@'
      },
      link: function($scope, $element) {
        var canvas = $element.find('canvas')[0];
        var ctx = canvas.getContext('2d');

        function redraw() {


          var ww = $scope.width;
          var hh = $scope.height;
          var o = $scope.open;
          var c = $scope.close;
          var h = $scope.high;
          var l = $scope.low;
          var color = $scope.color || '#333';

          ctx.fillStyle = color;
          ctx.clearRect(0, 0, ww, hh + 10);

          ctx.globalAlpha = 0.5;
          ctx.fillRect(0, 10, ww, hh);
          ctx.globalAlpha = 1;

          var tX = ww * (o - l) / (h - l);
          ctx.beginPath();
          ctx.lineTo(tX, 10);
          ctx.lineTo(tX + 5, 0);
          ctx.lineTo(tX - 5, 0);
          ctx.fill();
          ctx.closePath();

          ctx.fillRect(0, 10, ww * (c - l) / (h - l), hh);

        };
        $scope.$watch('open', redraw, true);
        $scope.$watch('high', redraw, true);
        $scope.$watch('close', redraw, true);
        $scope.$watch('low', redraw, true);
        $scope.$watch('color', redraw, true);
        redraw();
      }
    };
  });
})();